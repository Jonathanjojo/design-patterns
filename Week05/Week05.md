# Week 5 - Refactoring

## Overview

* Refactoring is the process of changing a software system in such a way that it doesnt alter the external behavior of the code, yet it improves the internal structures.
* Given a bad or even chaotic design, refactoring can rework it to a well designed code.
* Each step in refactoring is simple:
    * push code up/down a hierarchy
    * move fields to another class
    * rename variables or classes or methods
* Small steps/changes can radically improves the design during development process
* Refactoring lets sofrtware developers to improve the design during development process

## Why

* Improve the software design
* Makes softwares easier to understand
* Helps finding bugs
* Helps you to program faster

## When

* Rule of Three
    * Two instances of similiar code dont require refactoring, but when similiar code is used three times, it should be extracted to a new procedure
    * A new function is added
    * A bug needs to be found
    * Code Review

## Problems 

* Databases
    * Schema
    * Data migrations
* Changin interfaces:
    * Very often
    * Can be a problem in public API
* Designs that are hard to refactor

## Practices

* Before refactoring, provide tests which are essentials to assure the correctness of the code before and after being refactorred

## What : Code Smells

* Bloaters
    * Large monolothic structures
    * Long method, large class, data clumps, long param list
* Object Oriented Abusers
    * Misuse of OOP
    * Alternative classes with different interfaces, complex switch statements
* Change Preventers
    * If you want to change one thing, you have to change it on other parts
    * Parallel Inheritance
* Dispensables
    * Pointless things
    * Comments, duplicates, dead codes
* Couplers
    * Feature envy (more needed data on other classes), middle man, inappropriate intimacy

## Techniques

* Composing Methods
* Moving Features between Objects
* Organizing Data
* Simplifying Conditional Expressions
* Simplifying Method Calls
* Dealing with Generalizations