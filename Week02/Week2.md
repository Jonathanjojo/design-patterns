# Week 2
### Jonathan Christopher Jakub - 1706040151 - AP 192 A

## Iterator Pattern (Behavioural)

* The iterator pattern provides a way to access the elements of an aggregate object sequentially without exposing its underlying representation
* Basically it provides a shared and common way to iterate elements of an aggregate object. It doesnt care if the objects are stored in different data structure, as long as it supports an iterator: built-in or customized.
* Structure:
    ![Diagram](../Week02/Iterator_Diagram.png)  
* How it does what it does:
    * Have an Iterator interface (or just implement built-in Iterator interface)
    * An aggregate object should have a createIterator() method which return its coresponding iterator. Different data structures may require different implementation of iterators.
    * The iterator itself implements the Iterator interface. Thus, it should have hasNext(), next(), or remove() if needed. For example:  
        ![Iterator Example](../Week02/Iterator_Example.png)
    * Then the client can just call the aggregate object and retrieve its iterator to sequentially traverse the stored elements, no matter the structure.
* This pattern comes handy when it is required for 1 entity to handle multiple aggregate objects with different storage structure.

## Composite Pattern (Structural)

* The composite pattern allows you to compose objects into tree structures to represent part-whole hierarchies. Composite lets client treat individual objects and compositions of objects uniformly.
* It lets you to have a series of objects inside objects and so on. But you can still treat them in the same manner, although the results may differ between a composite and a single item.
* How it does what it does:
    * We design a tree like object. Each internal node is a composite item, and each leaf node is a single item.
    * We shall have an abstract class for the nodes of the "tree". Why abstract class? Abstract class lets us to set a default implementation: there are behaviours that is supported for composite items (for example, access child) but not the inverse, and vice versa.
    * The abstract class may have your required implementations (let it have add, remove, print, getChild, etc)
    * Composite items and single items extend the same abstract class we made earlier, but they both have different implementations:
        * Composites may have a storage of their children as attribute as they act as a container. It also implements the getChild() method. Its print() method calls its children's print() method, it may use its iterator to do this.
        * Single item doesnt have a storage for children as they dont have any. But it may have accessor for its attributes.
    * Now, we can plug in the iterator pattern to our composite pattern as well. We can create our abstract class to have createIterator() method.
    * Here is an example of an iterator for our composite items:
        ![Composite Iterator](../Week02/Composite_Iterator.png)
    * About the leaf nodes, it inherits the createIterator() method from its abstract class. We are faced with 2 choices, return null, or return an iterator whose hasNext() method always returns False. The later is ofcourse prefered as it diminishes the use of exception handling.

## Decorator Pattern (Structural)

* The Decorator Pattern attaches additional responsibilities to an object dynamically. Decorators provide a flexible alternative to subclassing for extending functionality.
* The decorator adds its own behavior either before and/or after delegating to the object it decorates to do the rest of the job.
* Given that the decorator has the same supertype as the object it decorates, we can pass around a decorated object in place of the original (wrapped) object.
* Pattern Diagram :
    ![Decorator Diagram](../Week02/Decorator_Diagram.png)
* How it does what it does:
    * We have an abstract class for concrete components AND decorators.
    * The decorators (concrete decorators extend this decorator class) and the components extend the same abstract class so it can be interchangable (between decorated or not)
    * Then the decorators HAS-A wrapped component (the same concrete component extending the abstract class earlier)
    * The magic happens in the decorators, since they both extend the same abstract class, they both have the same key method(s). For example they have behaviour called method(). In the decorator, method() is defined as wrappedComponent.method() + decorations, before or after or both.
    * Since they both extend the same class, the client can create Objects and wrap it indefinitely:
    ```java
    ComponentObj obj = new Component();
    obj = new Decorator1(obj);
    obj = new Decorator2(obj);
    ```

## Proxy Pattern (Structural)

* The proxy pattern provides a surrogate or placeholder for another object to control access to it.
* General structure of proxy pattern :  
    ![Proxy Structure](../Week02/Proxy_Structure.png)
* There are 4 common types of proxy:
    * Virtual proxy (Lazy initialization)
        * This is when you have a heavyweight service object that wastes system resources by being always up, even though you only need it from time to time.
        * Instead of creating the object when the app launches, you can delay the object’s initialization to a time when it’s really needed.
    * Protection proxy (Access control)
        * This is when you want only specific clients to be able to use the service object; for instance, when your objects are crucial parts of an operating system and clients are various launched applications (including malicious ones).
        * The proxy can pass the request to the service object only if the client’s credentials match some criteria.
    * Remote proxy (Local execution of a remote service)
        * This is when the service object is located on a remote server.
        * In this case, the proxy passes the client request over the network, handling all of the nasty details of working with the network
    * Caching proxy (Caching request results)
        * This is when you need to cache results of client requests and manage the life cycle of this cache, especially if results are quite large.
        * The proxy can implement caching for recurring requests that always yield the same results. The proxy may use the parameters of requests as the cache keys.

## Bridge Pattern (Structural)

* Bridge is a structural design pattern that lets you split a large class or a set of closely related classes into two separate hierarchies—abstraction and implementation—which can be developed independently of each other.
* Basically, rather than creating a single implementation of a concrete class, it is better to have an interface for that class, and an abstraction composing of the interface implementation of the object of the concrete class. Both of them will act as a bridge between the real object and client.
* Structure:   
    ![Bridge Structure](../Week02/Bridge_Structure.png)
* How it does what it does :
    * We have an interface for our original class (all of its methods are purely internal), then every concrete class shall implement this interface
    * To control/access it, we shall have a controller class composing of an object of above interface. All of its methods are modified control of the object we stored earlier. This class can be refined.
    * Now the client can control the object through a bridge that is our controller.