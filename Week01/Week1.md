# Week 1
### Jonathan Christopher Jakub - 1706040151 - AP 192 A

## Observer Pattern (Behavioural)

* Observer is a behavioral design pattern that lets you define a subscription mechanism to notify multiple objects about any events that happen to the object they’re observing.
* The observer pattern defines a one-to-many dependency between objects so than when one object change state, all its dependents are notified and updated automatically.
* There are 2 roles in play:
    * Publisher (Subject)
    * Subscriber (Observer)
* Objects can subscribe/unsubscribe from any publisher. Subscribed observers shall be notified about any update the publisher has got.
* How did it do it:
    * In the below example, Java built-in Observable is used. Basically it provides an Array List of observers which can add/remove observers. Notifications done by iterating the subscribed observers.
    * If the publisher wish to update, it has 2 choice : to notify its subscribers or not to notify them. Hence the setChange() method is developped. If it were True, the subscribers will be notified.
    * By notified, it means that certain behaviour of the subscribers are being called.
* Publisher example :  
    ![Publisher](Observer_Publisher.png)
* Subscriber example :  
    ![Observer](Observer_Observer.png)
* Usage example :  
    ![Usage](Observer_Usage.png)

## Command Pattern (Behavioural)

* Command is a behavioral design pattern that encapsulates request into a stand-alone object that contains all information about the request. This transformation lets you parameterize methods with different requests, delay or queue a request’s execution, and support undoable operations.
* In diagram:  
    ![Command Diagram](Command_Diagram.png)
* How it does:
    * The invoker has a storage of many commands (Object of Command interface). Commands have to have execute and undo method, each with their own implementation.
    * Empty storage is filled with a command which do nothing to safely implement the execute() method for all storage slot.
    * When a command in a slot is called, it calls the execute() method of that command.
    * Everytime a command is called, the undoCommand is assigned to that last called command to be called if the undo command is called.
    * Sometimes, a MacroCommand is needed. a MacroCommand is a command that runs multiple existing commands at once. It is just another Command object with a list of commands. When it is executed, it loops through all the commands and execute each one.
* Another usage of the Command Pattern is to create a limitable job queue. The thread only calls the jobs' execute method, but now it can limit how many jobs a thread should/could handle. At one time it can run a certain job, the other time it can run another job without any problem, as long as it implements the Command interface.
* Invoker example:  
    ![Invoker](Command_Invoker.png)
* Command example:  
    ![Command](Command_Command.png)

## Template Method Pattern (Behavioural)

* The template method pattern defines the skeleton of an algorithm in a method, deferring some steps to subclasses.
* This pattern redefine certain steps of an algorithm without changing its structure.
* Basically, it define the steps of an algorithm. But it provides subclasses' implementation for one or more steps.
* To realize this pattern, abstract class is used to give default implementation for some steps, wnad abstract methods for subclasses implementation on other steps.
* How it does:  
    ![Template Pseudo](Template_Pseudo.png)
    * It has an abstract class with concrete stepsFunction() which calls some other functions. Some may be abstract to be implemented by subclasses, some may be concrete.
    * The hook is optional method for the subclasses to override if they wish to do so.
* Example:  
    ![Template Example](Template_Example.png)

## State Pattern (Behavioural)

* The state pattern allows an object to alter its behaviour when its internal state changes.
* The object has states that has their own implemented behaviour, the state is saved as an attribute (HAS). The state itself HAS the object as well. It uses it to change the object state after going through a state.
* The state changes follow a predefined state-diagram. Each change of state is implemented at the preceeding state to decide which state to go next.
* Object example with state pattern:  
    ![Object State Example](State_Object_Example.png)
* State example:  
    ![State Example](State_State_Example.png)
* How it does:
    * Define a state interface and create concrete states of your need (implementing the state interface). Implement the methods of the concrete object class in the interface and the state classes.
    * Inside the state class, it HAS the concrete object as an attribute. Implement the state flow in the methods of the states. At points where the state changes due to method calls, then change the object's state in the attribute. Therefore you will need a state setter on the concrete object.
    * Inside the concrete object class, list all of the available states as its attributes (instantiated in the constructor)
    * Set the initial state in the constructor.
    * Define methods of the object. If the implementation of the method depends on the state, then call the state's method instead inside the method of the object.
    * Thus the object now reacts depends on its state and state modification is just as simple as creating another state class and add it to the object attribute.


## Chain of Responsibility Pattern (Behavioural)

* Chain of Responsibility is a behavioral design pattern that lets you pass requests along a chain of handlers. Upon receiving a request, each handler decides either to process the request or to pass it to the next handler in the chain.
* How it does:
    * We have a Handler interface, whoever has the ability to pass/handle request must implement this interface. Say it has the handle() method.
    * Lets say we have composites/inheritted objects.
    * Handling priority comes from the smallest refered object. Then inside the objects' handle() method, it decides whether to handle the request, or pass it to the parent class or to one of its composite object. 
    * The conditional can be as simple as possible. We can check whether the object in line has the ability/authority to handle it or not, if not then pass the responsibility to the next in line depends on the condition.
* Structure example:
    ![Chain of Responsibility Example](Chain_Example.png)


## Mediator Pattern (Behavioural)

* Mediator is a behavioral design pattern that lets you reduce chaotic dependencies between objects. The pattern restricts direct communications between the objects and forces them to collaborate only via a mediator object. It is like a crossroad where everyone has to pass it to get to the other line of the road.
* How it does:
    * We have a mediator class implementing mediator interface. It may have notify() method(s) or anything with similiar function.
    * The mediator class acts as the container of the components as well, it has the components as attributes. The mediator object applies the logic behind the notifications between objects, therefore it has the informations about the concrete components.
    * For every component that is going to communicate to other components/user, it has to have the mediator object as their composite object (attribute), therefore we set an abstract class which has the mediator object as attribute.
    * Whenever the components wish to notify other components/user, it calls the mediator object's method like : self.mediator.notify(sender, args)
* Example :
    ![Mediator Example](Mediator_Example.png)