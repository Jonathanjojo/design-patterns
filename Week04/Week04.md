# Week 4
### Jonathan Christopher Jakub - 1706040151 - AP 192 A

## Singleton Pattern (Creational)

* The singleton pattern restricts the instantiation of a class to one. This is useful when exactly one object is needed to coordinate actions across the system.
* Critics consider the singleton to be an anti-pattern in that it is frequently used in scenarios where it is not beneficial, introduces unnecessary restrictions in situations where a sole instance of a class is not actually required, and introduces global state into an application.
* There are two initialization: eager and lazy. 
* The first example below is an example of an eager init, having the object created when the class is loaded.
* Example implementation :
    ```java
    public final class Singleton {

        private static final Singleton INSTANCE = new Singleton();

        private Singleton() {}

        public static Singleton getInstance() {
            return INSTANCE;
        }
    }
    ```

* The second example below implements the lazy init, having the object created when it is called.
* Synchronize it when multiple sources try to instatiate it at the same time resulting of many instance being made. The synchronized keyword forces every thread to wait its turn before it can enter the method so no two threads can enter the method in the same time.

    ```java
    public final class Singleton {

    private static Singleton instance = null;

    private Singleton() {}

    public static synchronized Singleton getInstance() {
        if (instance == null) {
            instance = new Singleton();
            }
        }

        return instance;
    }
    ```
* How it does what it does:
    * The singleton class has a private static instance (lets call it INSTANCE). The static modifier makes it a class variable and the value will be carried throughout all the objects.
    * It HAS to have a PRIVATE constructor.
    * The instatiation is done via the getInstance() method which will return the INSTANCE (it will return the same object for all calls of getInstance) for the lazy init, and done when the class is loaded with eager init.
    * Since the returned instance is always the same object, singleton assures that the instantiation of the class through the getInstance() method is limited to only one.

## Builder Pattern (Creational)

* Builder is a creational design pattern that lets you construct complex objects step by step. The pattern allows you to produce different types and representations of an object using the same construction code.

* Structure:  
    ![Builder Structure](../Week04/Builder_Structure.png)

* How it does what it does:
    * Instead of bloating our constructor with numerous parameters and types, we provide a builder class (the class constructor can be the default constructor).
    * The builder class will have the to-be-created object as its attribute. Then we have a set of setter methods for that object AND a reset method.
    * If we wish to customize our to-be-created object, we set the prefered attributes using the setter (builder) methods.
    * When we are done, call the provided getResult() method to get our customized-multi-attribute object.

## MVC Pattern (Compound)

* MVC stands for Model, View, Controller : a triad of object model, logic, and displayer.
* The components:
    * The model uses Observer to keep the views updated on the latest state changes.
    * The view and controller use the Strategy pattern. The controller is the behaviour of the view (strategy) which can be easily exchanged with another controller if you wish for a different behaviour. 
    * The view itself uses the Composite Pattern to manages its display components.
* General structure:  
    ![MVC Structure](../Week04/MVC_Structure.png)
* In details, a recap on how the design patterns used works:
    * Model:
        * We shall have an interface for our models, and since it is a publisher, we shall provide methods to add, remove, and notify its subscribers.
        * Then we shall create concrete class(es) for the model. Dont forget to provide some mutators and accessors.
    * View:
        * The view is an observer (subscriber) of the model, therefore it shall have the observed object that is the model as its attribute and register itself to the model.
        * The view receives inputs from the user, so you shall implement the input methods and pass the calls from user to the controller.
        * The view implements the Strategy pattern with the controller as the staategies. This way we can plug in and out the logic for our views. Remembering the strategy pattern, you should have the controller as an attribute.
    * Controller
        * Our controller is some sort of manager for our model and view, it bridges the other two.
        * It is good to have an interface for our controller because it is a strategy for our view. Then create concrete class(es) for our controller(s).
        * Our controller shall have the view and model(s) as its attribute, because it is an observer for the model as well as the strategy for the view. For the view, inside the constructor of our controller, create the view for it.
        * Inside the controller class, create model states mutators and accessors. For accessors, you might want to pass the information to the view.
* Based on above description, client needs only to instantiate the model and the controller (pass the model into the controller) to make everything run.