# Week 0
### Jonathan Christopher Jakub - 1706040151 - AP 192 A

## OOP Review

### Structured vs Object-Oriented Programming

* Structured Programming
    * Chronologicaly procedural
    * Works on user event

* OOP
    * Works with object and class
    * The object has:
        * attributes/properties/states (Things that the object "know" or has properties of)
        * behaviours/methods (Things that the object capable of doing)
    * With those 2, the object can perform actions and store informations
    * Objects are created by instantiating class(es), which the class acts as a blueprint of the object being created
    * Real world problems are easier modelled with OOP

### The Benefits of OOP

* Modularity
    * Each class and object (and their each behaviours) can be modified individually
* Information Hiding (Encapsulation)
    * Each attributes/behaviours of each object can be restricted accordingly (mutation or/and access)
* Code Reuse
    * Classes are easy to reuse and being imported
* Pluggability & Debugging Case 
    * Since classes use to be modular and can stand on its own, it is easy to be plugged in and out.

### Priciples of OOP

* Encapsulation (Restricting Access)
* Inheritance (Inherit properties and behaviours while permitting modifications)
* Interface (Contract between components)
* Polymorphism (Vary behaviours across actual object)

## Introduction to Design Patterns

* Design patterns helps to prevent subtle issues that can cause major problems and improves code readability for coders and architects familiar with the patterns

* Using design pattern might reduce the gap between initial design and its actual implementation

* The objectives :
    * Readability and Maintainability
    * Extendsibility
    * Scalability
    * Testability
    * Reusability

* Mandatory Elements of design patterns (cont.): 
    * Name and Category
    * Problem
    * Solution
    * Consequences (Pros and Cons)

* Pros and Cons of using Design Patterns:
    * Pros
        * Consistent solution
        * Clarity of design
        * Improve time by providing template
        * Reusable through composition
    * Cons 
        * Might increase the object usage (memory usage)
        * Might increase runtime
        * Might increase the layers between objects
        * Consequences are subjective
        * Subject to different interpretation, thus miscommunication is a risk
        * Can be overused and abused -- Anti Patterns

* Example of design patterns:
    * Singleton (Limit instantiation to only 1)
    * Decorator (Wrap objects to provide modified behaviour)
    * Proxy (Control access to an object)
    * Adapter (Bridging incompatible interfaces with a new interface)
    * Facade (Combining set of interfaces and simplify it under single callable interface)

## Strategy Pattern 
  
![Class Strategy Pattern](Strategy.png)

* Between HAS-A, IS-A, and RELATED-TO, the HAS-A composition is prefered over inheritance.
* Creating systems using composition gives you a lot more flexibility, ability to encapsulate families of algorithm into their own classes while enabling you to still change the behaviour at runtime.
* Thus, the Strategy Pattern defines a family of algorithms, encapsulates each one, and make them interchangeable. It lets the algoritm to vary independently from clients that use it.
* How did it do it?
    * Each class HAS-A behavioural object class (in our case it is the fly and quack behaviour) which is bounded by an interface to have certain methods. These methods would have different implementation depending on the behavioural class it composed. The interchangability comes from the interface that bounds it to have an implementation of a behaviour PLUS a mutator that can change the behaviour at runtime.
* In short : Prefer composition of behavioural object class that affects the object behaviour RATHER THAN inherit it from a super class than override its method.

## Patterns in the Real World

* A Pattern is a solution to a RECURRING problem in certain context.
* The 3 definitions:   
    * Context :
        * Situation which the pattern applies
    * Problem :
        * Goal you are trying to achieve in the context
    * Solution :
        * General design everyone can apply which resolve the goal in the context
* Patterns mostly fall into one of these 3 categories :  
    ![Pattern Motives](PatternMotives.png)
* By the object it concerns, patterns can be divided to :  
    ![Pattern Objects](PatternObjects.png)  
* By the scope as well :  
    ![Pattern Scopes](PatternScopes.png)  
* Pattern should only be used when **fit**. Do not use pattern if you can come up to a more simple solution.
* A design pattern should have:
    * Name and Category
    * Intent (Short description)
    * Motivation (Concrete scenario of a problem and solution)
    * Applicability (Situations the pattern can be applied into)
    * Structures (Relation among the classes in use)
    * Participants (Classes and Objects in the design and their responsibility)
    * Collaboration (How the participants work with each other)
    * Consequences (Effect of using the pattern)
    * Sample Code
    * Known Uses (Real problem example)
    * Related Patterns
* With good patterns, there must be bad patterns -- Antipattern
* Antipattern = Documented bad design pattern :
    * Explain why bad solution is attractive 
    * Explain why the solution is bad in the long term
    * Suggest that other pattern may provide good solutions
* An antipattern should have:
    * Name
    * Problem and context
    * Force (the attractiveness)
    * Supposed solution (The bad yet attractive solution)
    * Refactored solution (The good pattern that could be used)
    * Example

* Example of patterns and their usages:  
    ![Patterns](Patterns.png)
