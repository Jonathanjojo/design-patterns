# Week 3
### Jonathan Christopher Jakub - 1706040151 - AP 192 A

## Factory Method Pattern (Creational)

* Factory pattern defines an interface for creating object, but lets subclasses decide which class to instantiate.
* It lets a class defer instantiation to sub classes
* Structure:   
    ![Factory Diagram](../Week03/Factory_Diagram.png)
* How it does what it does:
    * We have an abstract Factory class, which has a default implementation of some method (in this case, the processing() method), but an abstract method for creating objects.
    * Then, for every factory we may have, we extend the Factory class and implement the abstract create() method for that subclass only.
    * Each factory class may instantiate different concrete product. Each product has to follow an interface/abstract class to be able to be used by other classes - refering to the interface.
    * Our client should intantiate our factory to get and process the product.
* So basically, a factory pattern lets us have different decoupled factories: each is able to create different products which can be refered as a type of the same object. It does it by extending a factory abstract class, then modify the create() method to return its prefered object(s).

## Abstract Factory Method Pattern (Creational)

* Abstract factory pattern provides an interface for creating families of related or dependent objects without specifying their concrete classes.
* Structure :  
    ![Abstract Factory Diagram](../Week03/Abstract_Factory_Diagram.png)
* How it does what it does:
    * We have an abstract factory class. The factory will be used to create a number of components rather than one. So if any changes needed, we will have to change this abstract class.
    * The concrete factory then will extend the abstract factory, and return each components it is designed to create.
    * Our client can implement another factory method pattern to choose which factory will be used to create which full object it wants to return (remember, our factory returns components, not a whole usable object). It can do it by extending a factory abstract class, then modify the create() method.
* The difference between Factory and Abstract Factory Method pattern:
    * Factory returns one objects only. Abstract Factory returns a family of objects through a set of methods.
    * Factory method does it through inheritance (extending the abstract class), while Abstract Factory method does it through composition (the abstract class of the factory has the types of components stored as its attributes, then the concrete factory will decide the subclassing for the component)
* The abstract factory method pattern is an extention of factory method pattern. As you can see, the abstract factory class implements the factory pattern (it has abstract methods returning the components, but it is up to the subclasses to choose which subclass of the component to be returned).

## Prototype Pattern (Creational)

* Prototype is a creational design pattern that lets you copy existing objects without making your code dependent on their classes.
* We cant really copy an object by going through all its fields and copy them. Some may not be accessible, and some may implements certain interface or extending another class.
* This pattern will serve for any objects that supports cloning (clone() method). It will suit better than subclassing the object and it will create an object independent to the original one.
* Structure :  
    ![Prototype Structure](../Week03/Prototype_Structure.png)
* How it does what it does:
    * To make our class to support prototyping, we shall create another constructor to our class which takes an object of that class as a parameter.
    * Inside this new constructor, assign the attributes to match the attributes of the given object.
    * Then provide a clone() method, which return a new object of that class by calling the constructor we just made, and give the object ITSELF as the parameter.
    * Same thing goes to the subclasses of the class, repeat the previous steps but call super(parameter) on the constructor.

## Adapter Pattern (Structural)

* The adapter pattern coverts the interface of a class into another interface the clients expect. Adapter lets classes work together that couldnt otherwise because of incompatible interface
* The usage of adapters lets no change of code to the existing interfaces. It just add a "bridge/middleman" between the interfaces so they could work together.
* Example :
    * Say we have a Duck interface with quack() and fly() methods. We also have a Turkey interface with gobble() and fly() methods.
    * We want our turkeys to take place as ducks, but they have different interfaces. So we make an adapter like so:  
    ![Adapter Example](../Week03/Adapter_Example.png)
* How it does what it does:
    * Say we got 2 different interface, we have to choose one interface to be adapted INTO, say its interface A and the interface B will be adapted to A.
    * Create a new adapter class implementing interface A and satisfy the methods. This class constructor takes objects of B.
    * At runtime, we can take objects of B, put it in our adapter class and run it just like we run objects of A.

## Facade Pattern (Structural)

* The facade pattern alters interfaces just like adapter pattern, but for different purpose: to simplify and hide the complexity.
* Basically, it calls the subsystems and all of its complex running with simple command from the client without restricting access to the individual subsystems.
* So, the Facade pattern provides a unified interface to a set of interfaces in a subsystem. It defines a higher level interface that makes the subsystem is easier to use in groups.
* Long story short, its a method that calls the other (lots of) subsystems/components methods.
* It bridges the client to all the subsystems in an easy to use interface.
* Here is a simple example:  
    ![Facade Example](../Week03/Facade_Example.png)
* How it does what it does:
    * We simply made a "bridge" class that HAS all of the subsystems as it components.
    * We compose a method, which calls the components (subsystems) methods.
    * Therefore we simplify the calling of the system. It is recommended to approach repeated callings of the subsystems.

## Flyweight Pattern (Structural)

* Flyweight is a structural design pattern that lets you fit more objects into the available amount of RAM by sharing common parts of state between multiple objects instead of keeping all of the data in each object.
* Lets say we have an object. The object's constant data is usually called the intrinsic state. It lives within the object; other objects can only read it, not change it. The rest of the object’s state, often altered “from the outside” by other objects, is called the extrinsic state.
* This pattern suggests that we should not store the extrinsic states, but rather pass it to the methods that rely on it. Thus, we can reuse the intrinsic parts for different contexts.
* Structure:  
    ![Flyweight Structure](../Week03/Flyweight_Structure.png)
* How it does what it does:
    * Rather than storing all the data within a single object, we just states the unique ones
    * And for common states, we can create a whole new object (its like a TYPE object that you call everytime you need data from it). So, objects with common shared states will have the same TYPE.
    * Provide a factory for TYPE objects as well, for already created TYPES, you can store it in an array and just return them if any object needs the type, otherwise, create another one.
