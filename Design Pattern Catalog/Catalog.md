# Design Pattern Personal Catalog

#### Acknowledgments
* This personal notes is based on:
    * Head First Design Pattern (Book by Elisabeth Freeman and Kathy Sierra)
    * [Refactoring Guru](https://refactoring.guru/design-patterns/)
    * Personal intrepretations

## Strategy Pattern (Behavioural)

* Strategy is a behavioral design pattern that lets you define a family of algorithms, put each of them into a separate class, and make their objects interchangeable.  
    ![Class Strategy Pattern](../Week00/Strategy.png)
* Between HAS-A, IS-A, and RELATED-TO, the HAS-A composition is prefered over inheritance.
* Creating systems using composition gives you a lot more flexibility, ability to encapsulate families of algorithm into their own classes while enabling you to still change the behaviour at runtime.
* Thus, the Strategy Pattern defines a family of algorithms, encapsulates each one, and make them interchangeable. It lets the algorithm to vary independently from clients that use it.
* How it does what it does?
    * The class HAS-A behavioural concrete object (in our case it is the fly and quack behaviour) which is bounded by an interface to have certain methods. These methods would have different implementation depending on the behavioural class it composed. The interchangability comes from the interface that bounds it to have an implementation of a behaviour PLUS a mutator that can change the behaviour at runtime.
* Conclusion :
    * Prefer composition of behavioural object then call the object behaviour RATHER THAN inherit the behaviour itself from a super class than override its method.

## Observer Pattern (Behavioural)

* Observer is a behavioral design pattern that lets you define a subscription mechanism to notify multiple objects about any events that happen to the object they’re observing.
* The observer pattern defines a one-to-many dependency between objects so than when one object change state, all its dependents are notified and updated automatically.
* There are 2 roles in play:
    * Publisher (Subject)
    * Subscriber (Observer)
* Objects can subscribe/unsubscribe from any publisher. Subscribed observers shall be notified about any update the publisher has got.
* How it does what it does:
    * In the below example, Java built-in Observable is used. Basically it provides an Array List of observers which can add/remove observers. Notifications done by iterating the subscribed observers.
    * If the publisher wish to update, it has 2 choice : to notify its subscribers or not to notify them. Hence the setChange() method is developped. If it were True, the subscribers will be notified.
    * By notified, it means that certain behaviour of the subscribers are being called.
* Publisher example :  
    ![Publisher](../Week01/Observer_Publisher.png)
* Subscriber example :  
    ![Observer](../Week01/Observer_Observer.png)
* Usage example :  
    ![Usage](../Week01/Observer_Usage.png)

## Command Pattern (Behavioural)

* Command is a behavioral design pattern that encapsulates request into a stand-alone object that contains all information about the request. This transformation lets you parameterize methods with different requests, delay or queue a request’s execution, and support undoable operations.
* In diagram:  
    ![Command Diagram](../Week01/Command_Diagram.png)
* How it does what it does:
    * The invoker has a storage of many commands (Object of Command interface). Commands have to have execute and undo method, each with their own implementation.
    * Empty storage is filled with a command which do nothing to safely implement the execute() method for all storage slot.
    * When a command in a slot is called, it calls the execute() method of that command.
    * Everytime a command is called, the undoCommand is assigned to that last called command to be called if the undo command is called.
    * Sometimes, a MacroCommand is needed. a MacroCommand is a command that runs multiple existing commands at once. It is just another Command object with a list of commands. When it is executed, it loops through all the commands and execute each one.
* Another usage of the Command Pattern is to create a limitable job queue. The thread only calls the jobs' execute method, but now it can limit how many jobs a thread should/could handle. At one time it can run a certain job, the other time it can run another job without any problem, as long as it implements the Command interface.
* Invoker example:  
    ![Invoker](../Week01/Command_Invoker.png)
* Command example:  
    ![Command](../Week01/Command_Command.png)

## Singleton Pattern (Creational)

* The singleton pattern restricts the instantiation of a class to one. This is useful when exactly one object is needed to coordinate actions across the system.
* Critics consider the singleton to be an anti-pattern in that it is frequently used in scenarios where it is not beneficial, introduces unnecessary restrictions in situations where a sole instance of a class is not actually required, and introduces global state into an application.
* There are two initialization: eager and lazy. 
* The first example below is an example of an eager init, having the object created when the class is loaded.
* Example implementation :
    ```java
    public final class Singleton {

        private static final Singleton INSTANCE = new Singleton();

        private Singleton() {}

        public static Singleton getInstance() {
            return INSTANCE;
        }
    }
    ```

* The second example below implements the lazy init, having the object created when it is called.
* Synchronize it when multiple sources try to instatiate it at the same time resulting of many instance being made. The synchronized keyword forces every thread to wait its turn before it can enter the method so no two threads can enter the method in the same time.

    ```java
    public final class Singleton {

    private static Singleton instance = null;

    private Singleton() {}

    public static synchronized Singleton getInstance() {
        if (instance == null) {
            instance = new Singleton();
            }
        }

        return instance;
    }
    ```
* How it does what it does:
    * The singleton class has a private static instance (lets call it INSTANCE). The static modifier makes it a class variable and the value will be carried throughout all the objects.
    * It HAS to have a PRIVATE constructor.
    * The instatiation is done via the getInstance() method which will return the INSTANCE (it will return the same object for all calls of getInstance) for the lazy init, and done when the class is loaded with eager init.
    * Since the returned instance is always the same object, singleton assures that the instantiation of the class through the getInstance() method is limited to only one.

## Decorator Pattern (Structural)

* The Decorator Pattern attaches additional responsibilities to an object dynamically. Decorators provide a flexible alternative to subclassing for extending functionality.
* The decorator adds its own behavior either before and/or after delegating to the object it decorates to do the rest of the job.
* Given that the decorator has the same supertype as the object it decorates, we can pass around a decorated object in place of the original (wrapped) object.
* Pattern Diagram :
    ![Decorator Diagram](../Week02/Decorator_Diagram.png)
* How it does what it does:
    * We have an abstract class for concrete components AND decorators.
    * The decorators (concrete decorators extend this decorator class) and the components extend the same abstract class so it can be interchangable (between decorated or not)
    * Then the decorators HAS-A wrapped component (the same concrete component extending the abstract class earlier)
    * The magic happens in the decorators, since they both extend the same abstract class, they both have the same key method(s). For example they have behaviour called method(). In the decorator, method() is defined as wrappedComponent.method() + decorations, before or after or both.
    * Since they both extend the same class, the client can create Objects and wrap it indefinitely:
    ```java
    ComponentObj obj = new Component();
    obj = new Decorator1(obj);
    obj = new Decorator2(obj);
    ```

## Proxy Pattern (Structural)

* The proxy pattern provides a surrogate or placeholder for another object to control access to it.
* General structure of proxy pattern :  
    ![Proxy Structure](../Week02/Proxy_Structure.png)
* There are 4 common types of proxy:
    * Virtual proxy (Lazy initialization)
        * This is when you have a heavyweight service object that wastes system resources by being always up, even though you only need it from time to time.
        * Instead of creating the object when the app launches, you can delay the object’s initialization to a time when it’s really needed.
    * Protection proxy (Access control)
        * This is when you want only specific clients to be able to use the service object; for instance, when your objects are crucial parts of an operating system and clients are various launched applications (including malicious ones).
        * The proxy can pass the request to the service object only if the client’s credentials match some criteria.
    * Remote proxy (Local execution of a remote service)
        * This is when the service object is located on a remote server.
        * In this case, the proxy passes the client request over the network, handling all of the nasty details of working with the network
    * Caching proxy (Caching request results)
        * This is when you need to cache results of client requests and manage the life cycle of this cache, especially if results are quite large.
        * The proxy can implement caching for recurring requests that always yield the same results. The proxy may use the parameters of requests as the cache keys.

## Template Method Pattern (Behavioural)

* The template method pattern defines the skeleton of an algorithm in a method, deferring some steps to subclasses.
* This pattern redefine certain steps of an algorithm without changing its structure.
* Basically, it define the steps of an algorithm. But it provides subclasses' implementation for one or more steps.
* To realize this pattern, abstract class is used to give default implementation for some steps, wnad abstract methods for subclasses implementation on other steps.
* How it does what it does:  
    ![Template Pseudo](../Week01/Template_Pseudo.png)
    * It has an abstract class with concrete stepsFunction() which calls some other functions. Some may be abstract to be implemented by subclasses, some may be concrete.
    * The hook is optional method for the subclasses to override if they wish to do so.
* Example:  
    ![Template Example](../Week01/Template_Example.png)

## State Pattern (Behavioural)

* The state pattern allows an object to alter its behaviour when its internal state changes.
* The object has states that has their own implemented behaviour, the state is saved as an attribute (HAS). The state itself HAS the object as well. It uses it to change the object state after going through a state.
* The state changes follow a predefined state-diagram. Each change of state is implemented at the preceeding state to decide which state to go next.
* Object example with state pattern:
    ![Object State Example](../Week01/State_Object_Example.png)
* State example:
    ![State Example](../Week01/State_State_Example.png)
* How it does what it does:
    * Define a state interface and create concrete states of your need (implementing the state interface). Implement the methods of the concrete object class in the interface and the state classes.
    * Inside the state class, it HAS the concrete object as an attribute. Implement the state flow in the methods of the states. At points where the state changes due to method calls, then change the object's state in the attribute. Therefore you will need a state setter on the concrete object.
    * Inside the concrete object class, list all of the available states as its attributes (instantiated in the constructor)
    * Set the initial state in the constructor.
    * Define methods of the object. If the implementation of the method depends on the state, then call the state's method instead inside the method of the object.
    * Thus the object now reacts depends on its state and state modification is just as simple as creating another state class and add it to the object attribute.

## Chain of Responsibility Pattern (Behavioural)

* Chain of Responsibility is a behavioral design pattern that lets you pass requests along a chain of handlers. Upon receiving a request, each handler decides either to process the request or to pass it to the next handler in the chain.
* How it does what it does:
    * We have a Handler interface, whoever has the ability to pass/handle request must implement this interface. Say it has the handle() method.
    * Lets say we have composites/inheritted objects.
    * Handling priority comes from the smallest refered object. Then inside the objects' handle() method, it decides whether to handle the request, or pass it to the parent class or to one of its composite object. 
    * The conditional can be as simple as possible. We can check whether the object in line has the ability/authority to handle it or not, if not then pass the responsibility to the next in line depends on the condition.
* Structure example:
    ![Chain of Responsibility Example](../Week01/Chain_Example.png)

## Mediator Pattern (Behavioural)

* Mediator is a behavioral design pattern that lets you reduce chaotic dependencies between objects. The pattern restricts direct communications between the objects and forces them to collaborate only via a mediator object. It is like a crossroad where everyone has to pass it to get to the other line of the road.
* How it does what it does:
    * We have a mediator class implementing mediator interface. It may have notify() method(s) or anything with similiar function.
    * The mediator class acts as the container of the components as well, it has the components as attributes. The mediator object applies the logic behind the notifications between objects, therefore it has the informations about the concrete components.
    * For every component that is going to communicate to other components/user, it has to have the mediator object as their attribute, therefore we set an abstract class which has the mediator object as attribute.
    * Whenever the components wish to notify other components/user, it calls the mediator object's method like : self.mediator.notify(sender, args)
* Example :
    ![Mediator Example](../Week01/Mediator_Example.png)

## Iterator Pattern (Behavioural)

* The iterator pattern provides a way to access the elements of an aggregate object sequentially without exposing its underlying representation
* Basically it provides a shared and common way to iterate elements of an aggregate object. It doesnt care if the objects are stored in different data structure, as long as it supports an iterator: built-in or customized.
* Structure:
    ![Diagram](../Week02/Iterator_Diagram.png)  
* How it does what it does:
    * Have an Iterator interface (or just implement built-in Iterator interface)
    * An aggregate object should have a createIterator() method which return its coresponding iterator. Different data structures may require different implementation of iterators.
    * The iterator itself implements the Iterator interface. Thus, it should have hasNext(), next(), or remove() if needed. For example:  
        ![Iterator Example](../Week02/Iterator_Example.png)
    * Then the client can just call the aggregate object and retrieve its iterator to sequentially traverse the stored elements, no matter the structure.
* This pattern comes handy when it is required for 1 entity to handle multiple aggregate objects with different storage structure.

## Composite Pattern (Structural)

* The composite pattern allows you to compose objects into tree structures to represent part-whole hierarchies. Composite lets client treat individual objects and compositions of objects uniformly.
* It lets you to have a series of objects inside objects and so on. But you can still treat them in the same manner, although the results may differ between a composite and a single item.
* How it does what it does:
    * We design a tree like object. Each internal node is a composite item, and each leaf node is a single item.
    * We shall have an abstract class for the nodes of the "tree". Why abstract class? Abstract class lets us to set a default implementation: there are behaviours that is supported for composite items (for example, access child) but not the inverse, and vice versa.
    * The abstract class may have your required implementations (let it have add, remove, print, getChild, etc)
    * Composite items and single items extend the same abstract class we made earlier, but they both have different implementations:
        * Composites may have a storage of their children as attribute as they act as a container. It also implements the getChild() method. Its print() method calls its children's print() method, it may use its iterator to do this.
        * Single item doesnt have a storage for children as they dont have any. But it may have accessor for its attributes.
    * Now, we can plug in the iterator pattern to our composite pattern as well. We can create our abstract class to have createIterator() method.
    * Here is an example of an iterator for our composite items:
        ![Composite Iterator](../Week02/Composite_Iterator.png)
    * About the leaf nodes, it inherits the createIterator() method from its abstract class. We are faced with 2 choices, return null, or return an iterator whose hasNext() method always returns False. The later is ofcourse prefered as it diminishes the use of exception handling.

## Bridge Pattern (Structural)

* Bridge is a structural design pattern that lets you split a large class or a set of closely related classes into two separate hierarchies—abstraction and implementation—which can be developed independently of each other.
* Basically, rather than creating a single implementation of a concrete class, it is better to have an interface for that class, and an abstraction composing of the interface implementation of the object of the concrete class. Both of them will act as a bridge between the real object and client.
* Structure:   
    ![Bridge Structure](../Week02/Bridge_Structure.png)
* How it does what it does :
    * We have an interface for our original class (all of its methods are purely internal), then every concrete class shall implement this interface
    * To control/access it, we shall have a controller class composing of an object of above interface. All of its methods are modified control of the object we stored earlier. This class can be refined.
    * Now the client can control the object through a bridge that is our controller.

## Factory Method Pattern (Creational)

* Factory pattern defines an interface for creating object, but lets subclasses decide which class to instantiate.
* It lets a class defer instantiation to sub classes
* Structure:   
    ![Factory Diagram](../Week03/Factory_Diagram.png)
* How it does what it does:
    * We have an abstract Factory class, which has a default implementation of some method (in this case, the processing() method), but an abstract method for creating objects.
    * Then, for every factory we may have, we extend the Factory class and implement the abstract create() method for that subclass only.
    * Each factory class may instantiate different concrete product. Each product has to follow an interface/abstract class to be able to be used by other classes - refering to the interface.
    * Our client should intantiate our factory to get and process the product.
* So basically, a factory pattern lets us have different decoupled factories: each is able to create different products which can be refered as a type of the same object. It does it by extending a factory abstract class, then modify the create() method.

## Abstract Factory Method Pattern (Creational)

* Abstract factory pattern provides an interface for creating families of related or dependent objects without specifying their concrete classes.
* Structure :  
    ![Abstract Factory Diagram](../Week03/Abstract_Factory_Diagram.png)
* How it does what it does:
    * We have an abstract factory class. The factory will be used to create a number of components rather than one. So if any changes needed, we will have to change this abstract class.
    * The concrete factory then will extend the abstract factory, and return each components it is designed to create.
    * Our client can implement another factory method pattern to choose which factory will be used to create which full object it wants to return (remember, our factory returns components, not a whole usable object). It can do it by extending a factory abstract class, then modify the create() method.
* The difference between Factory and Abstract Factory Method pattern:
    * Factory returns one objects only. Abstract Factory returns a family of objects through a set of methods.
    * Factory method does it through inheritance (extending the abstract class), while Abstract Factory method does it through composition (the abstract class of the factory has the types of components stored as its attributes, then the concrete factory will decide the subclassing for the component)
* The abstract factory method pattern is an extention of factory method pattern. As you can see, the abstract factory class implements the factory pattern (it has abstract methods returning the components, but it is up to the subclasses to choose which subclass of the component to be returned).

## Adapter Pattern (Structural)

* The adapter pattern coverts the interface of a class into another interface the clients expect. Adapter lets classes work together that couldnt otherwise because of incompatible interface
* The usage of adapters lets no change of code to the existing interfaces. It just add a "bridge/middleman" between the interfaces so they could work together.
* Example :
    * Say we have a Duck interface with quack() and fly() methods. We also have a Turkey interface with gobble() and fly() methods.
    * We want our turkeys to take place as ducks, but they have different interfaces. So we make an adapter like so:  
    ![Adapter Example](../Week03/Adapter_Example.png)
* How it does what it does:
    * Say we got 2 different interface, we have to choose one interface to be adapted INTO, say its interface A and the interface B will be adapted to A.
    * Create a new adapter class implementing interface A and satisfy the methods. This class constructor takes objects of B.
    * At runtime, we can take objects of B, put it in our adapter class and run it just like we run objects of A.

## Facade Pattern (Structural)

* The facade pattern alters interfaces just like adapter pattern, but for different purpose: to simplify and hide the complexity.
* Basically, it calls the subsystems and all of its complex running with simple command from the client without restricting access to the individual subsystems.
* So, the Facade pattern provides a unified interface to a set of interfaces in a subsystem. It defines a higher level interface that makes the subsystem is easier to use in groups.
* Long story short, its a method that calls the other (lots of) subsystems/components methods.
* It bridges the client to all the subsystems in an easy to use interface.
* Here is a simple example:  
    ![Facade Example](../Week03/Facade_Example.png)
* How it does what it does:
    * We simply made a "bridge" class that HAS all of the subsystems as it components.
    * We compose a method, which calls the components (subsystems) methods.
    * Therefore we simplify the calling of the system. It is recommended to approach repeated callings of the subsystems.

## Prototype Pattern (Creational)

* Prototype is a creational design pattern that lets you copy existing objects without making your code dependent on their classes.
* We cant really copy an object by going through all its fields and copy them. Some may not be accessible, and some may implements certain interface or extending another class.
* This pattern will serve for any objects that supports cloning (clone() method). It will suit better than subclassing the object and it will create an object independent to the original one.
* Structure :  
    ![Prototype Structure](../Week03/Prototype_Structure.png)
* How it does what it does:
    * To make our class to support prototyping, we shall create another constructor to our class which takes an object of that class as a parameter.
    * Inside this new constructor, assign the attributes to match the attributes of the given object.
    * Then provide a clone() method, which return a new object of that class by calling the constructor we just made, and give the object ITSELF as the parameter.
    * Same thing goes to the subclasses of the class, repeat the previous steps but call super(parameter) on the constructor.

## Flyweight Pattern (Structural)

* Flyweight is a structural design pattern that lets you fit more objects into the available amount of RAM by sharing common parts of state between multiple objects instead of keeping all of the data in each object.
* Lets say we have an object. The object's constant data is usually called the intrinsic state. It lives within the object; other objects can only read it, not change it. The rest of the object’s state, often altered “from the outside” by other objects, is called the extrinsic state.
* This pattern suggests that we should not store the extrinsic states, but rather pass it to the methods that rely on it. Thus, we can reuse the intrinsic parts for different contexts.
* Structure:  
    ![Flyweight Structure](../Week03/Flyweight_Structure.png)
* How it does what it does:
    * Rather than storing all the data within a single object, we just states the unique ones
    * And for common states, we can create a whole new object (its like a TYPE object that you call everytime you need data from it). So, objects with common shared states will have the same TYPE.
    * Provide a factory for TYPE objects as well, for already created TYPES, you can store it in an array and just return them if any object needs the type, otherwise, create another one (caching).

## Builder Pattern (Creational)

* Builder is a creational design pattern that lets you construct complex objects step by step. The pattern allows you to produce different types and representations of an object using the same construction code.

* Structure:  
    ![Builder Structure](../Week04/Builder_Structure.png)

* How it does what it does:
    * Instead of bloating our constructor with numerous parameters and types, we provide a builder class (the class constructor can be the default constructor).
    * The builder class will have the to-be-created object as its attribute. Then we have a set of setter methods for that object AND a reset method.
    * If we wish to customize our to-be-created object, we set the prefered attributes using the setter (builder) methods.
    * When we are done, call the provided getResult() method to get our customized-multi-attribute object.

## MVC Pattern (Compound)

* MVC stands for Model, View, Controller : a triad of object model, logic, and displayer.
* The components:
    * The model uses Observer to keep the views updated on the latest state changes.
    * The view and controller use the Strategy pattern. The controller is the behaviour of the view (strategy) which can be easily exchanged with another controller if you wish for a different behaviour. 
    * The view itself uses the Composite Pattern to manages its display components.
* General structure:  
    ![MVC Structure](../Week04/MVC_Structure.png)
* In details, a recap on how the design patterns used works:
    * Model:
        * We shall have an interface for our models, and since it is a publisher, we shall provide methods to add, remove, and notify its subscribers.
        * Then we shall create concrete class(es) for the model. Dont forget to provide some mutators and accessors.
    * View:
        * The view is an observer (subscriber) of the model, therefore it shall have the observed object that is the model as its attribute and register itself to the model.
        * The view receives inputs from the user, so you shall implement the input methods and pass the calls from user to the controller.
        * The view implements the Strategy pattern with the controller as the staategies. This way we can plug in and out the logic for our views. Remembering the strategy pattern, you should have the controller as an attribute.
    * Controller
        * Our controller is some sort of manager for our model and view, it bridges the other two.
        * It is good to have an interface for our controller because it is a strategy for our view. Then create concrete class(es) for our controller(s).
        * Our controller shall have the view and model(s) as its attribute, because it is an observer for the model as well as the strategy for the view. For the view, inside the constructor of our controller, create the view for it.
        * Inside the controller class, create model states mutators and accessors. For accessors, you might want to pass the information to the view.
* Based on above description, client needs only to instantiate the model and the controller (pass the model into the controller) to make everything run.